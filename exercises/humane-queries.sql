-- Name:
-- GTID:
-- gtLogin:

use humane;


-- Write SQL queries to answer the following questions based on the
-- humane schema in humane-schema.sql. Do not hard-code any primary or
-- foreign key values.

-- Run the humane-schema.sql and humane-data.sql scripts if you want
-- to experiment with data.

-- Queries:

-- What are the names of the pets at Howell Mill?
select 'Names of the pets at Howell Mill:' as '';

-- What is the name of the manager of Mansell?
select 'Name of the manager of Mansell:' as '';

-- What are the names of the workers who work at Mansell?
select 'Names of the workers who work at Mansell:' as '';

-- What are the names of the workers who are volunteers?
select 'Names of the workers who are volunteers:' as '';

-- What are the names of the workers who work on Saturdays at Mansell?
select 'Names of the workers who work on Saturdays at Mansell:' as '';

-- What are the names of the shelters and the numbers of workers who work at them?
select 'Names of the shelters and the numbers of workers who work at them:' as '';
