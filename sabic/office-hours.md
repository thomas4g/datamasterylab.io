---
layout: sabic
title: SABIC Intro Computing - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours located in CoC 133

- Mondays, 15:30 - 17:30
- Thursdays, 12:30 - 14:00
- Other times by appt.

Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or one of my TA labs (CoC 107) if I'm not expecting anyone.
