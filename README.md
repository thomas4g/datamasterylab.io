# datamastery.github.io

## Large Files

We use git-lfs for large files (> 50 MB). Follow installation instructions here: https://git-lfs.github.com/

When adding a large data file, say `data/big_data.csv`, follow these steps:

```sh
git lfs track "data/big_data.csv"
git add .gitattributes
git add "data/big_data.csv"
git commit -m "Add data/big_data.csv, tracked by git-lfs"
git push
```
