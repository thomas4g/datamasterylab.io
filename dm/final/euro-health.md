---
layout: homework
title: Euro Health
---

# Euro Health


## Introduction

In this assignment you will practice using Pandas, Numpy, Matplotlib and Jupyter Notebooks.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking this course, the TA's and the lecturer. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, Canvas ID and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.


## Problem Description

You're interested in the health practices and outcomes of developed nations, particularly the EU and US.

## Solution Description

Download [`euro-health.csv`](euro-health.csv), which contains health-related information derived from data in the [Eurostat](http://ec.europa.eu/eurostat/) [database](http://ec.europa.eu/eurostat/data/database) (Expand the tree to find the data tables). For comparison to the US, I added roughly equivalent statistics for the US. US data come from the following sources:

- https://www.cdc.gov/nchs/data/databriefs/db293.pdf
- https://www.niddk.nih.gov/health-information/health-statistics/overweight-obesity
- https://www.cdc.gov/tobacco/data_statistics/fact_sheets/adult_data/cig_smoking/index.htm
- https://www.cdc.gov/media/releases/2017/p1116-fruit-vegetable-consumption.html
- https://www.cdc.gov/nchs/fastats/exercise.htm

The data file has the following columns:

- ex - percentage of population doing aerobic exercise more thean 150 minutes per week
- fv - percentage of population eating at least 5 servings of fruits and vegetables a day
- le - life expectancy at one year (remaining years to live)
- obese - percentage of population with [BME](https://www.cdc.gov/obesity/adult/defining.html) > 30
- sk - percentage of adults who smoke occasionally or daily ("current smokers")

In the same directory as this data file write a Jupyter Notebook named `<login-id>-euro-health`, where <login-id> is your Canvas login ID, e.g., `jdoe3`.

### Jupyter Notebook Sections

Your notebook should have the following parts:

#### Part 0 - Header

Include a Markdown cell at the top of your notebook with your name, Canvas ID and GTID.

#### Part 1 - Basic Metrics

Make the necessary imports and read the CSV data file into a Pandas DataFrame named `health`. Use the first column from the data file (the countries) as the index column for the DataFrame. Answer the questions below using the DataFrame.

For each of the following, include a Markdown cell with the question followed by a code cell which computes and displays the answer. For example, for the question

- What is 2 * 3?

You would have a Markdown cell with:

```
What is 2 * 3?
```

followed by a code cell with

```
2 * 3
```

For each of the data columns, what is the average, which country is "best", which is "worst" and how does the US compare (where would US rank compared to EU countries.

- What is the average life expectancy of EU countries in the data set?
- In which country is the life expectacy highest?
- In which country is the life expectacy lowest?
- Where does the US rank in life expectancy compared to EU countries?
- What is the average obesity rate of EU countries in the data set?
- Which country is most obese?
- Which country is least obese?
- Where does the US rank in obesity rate compared to EU countries?
- What is the average smoking rate of EU countries in the data set?
- Which country smokes the most?
- Which country mokes the least?
- Where does the US rank in smoking rate compared to EU countries?
- What is the average vegetabel-eating rate of EU countries in the data set?
- Which country eats the most vegetables?
- Which country eats the least vegetables?
- Where does the US rank in fruit and vegetable consumption compared to EU countries?

What do these results tell you about health outcomes in Europe and in the US?

#### Part 2 - Visualization (Extra Credit)

For each of the following, include a Markdown cell with the question/description and a code cell that produces the visualization. Choose a graphic display that would clearly present the information.

One variable (1 points each):

- Country life expectancy relative to other countries; which country has highest, second-highest, etc. -- by how much
- Country healthy life expectancy relative to other countries; which country has highest, second-highest, etc. -- by how much
- Country obesity rates relative to other countries; which country is most obese, second-most, etc. -- by how much
- Country smoking rates relative to other countries; which country smokes the most, second-most, etc. -- by how much
- Country vegetable eating rates relative to other countries; which country eats most, second-most, etc. -- by how much
- Country exercise rates relative to other countries; which country exercises most, second-most, etc. -- by how much

What do these plots tell you about the gaps between countries in the data set?

Two variables: (2 points each)

Rather than looking at the measurements by country, look at the measurements relative to other measurements. Each pair of measurements in the data set is paired by country, that is, associated. Plot life expectancy against each of the following varibales (life expectancy should be the dependent variable).

- Obesity rate
- Smoking rate
- Vegetable-eating rate
- Exercise-rate

What do these plots tell you about these risk factors?

## Tips and Considerations

- Some data are missing in the data file (as they are in the source data files). See the course slides for tips on how to handle the missing data.


## Discussion

I created the relatively clean dataset for this project from these source files downloaded from Eurostat (they're all [gzip](http://www.gzip.org/) archives, as downloaded from Eurostat):

-


## Turn-in Procedure

Submit your `<login-id>-euro-health.ipynb` file to this homework assignment on Canvas. DO NOT submit the `apcdata_week.csv` data file! After you turn in your files, make sure to check that you have submitted the correct files that you intended to. Do not wait unti lthe last minute to submit your assignment!

Good luck!
