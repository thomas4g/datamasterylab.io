---
layout: homework
title: HW6 - MARTA Database GUI
---

# HW6 - MARTA Database GUI

## Introduction

In this exercise you will practice:

- writing GUI applications with PyQt, and
- writing Python code to interact with an SQL database.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking CS 2316, the TA's and the professor. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, Canvas login ID, and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all modules, classes methods, and functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.

## Problem Description

You need to share your MARTA database with users who don't know SQL.

## Solution Description

Create a GUI application in a Python source file named `marta_gui.py` that displays the table data in the marta database you created in the previous homework.

### Set-up

To be sure we're all working from the same database schema, download these three files into the same directory:

- [marta-schema.sql](marta-schema.sql)
- [passenger_data.csv](../hw4/passenger_data.csv)
- [import_marta.py](import_marta.py)

Once you've downloaded these files, make sure your MySQL server is running and execute these commands on your operating system command line:

```sh
mysql -u root < marta-schema.sql
python import_marta.py passenger_data.csv marta localhost root
```

After that last command you'll be prompted for the password of your MySQL server's root user.

### Main Screen

Your application may assume that there is a MySQL database named `marta` that has been created and populated using the set-up scripts above. `marta_gui.py` should connect to this database and display a main screen as described below. To connect to the database you can hard code `localhost`, `root`, `marta` and `""` for the host, user, database and root password. If you use a different DB user or password on your computer you'll have to add a way to customize those settings when you run your script on your computer, but we will assume the hard-coded values above when we grade on our computers.

The main screen should show three buttons labeled:

- "Routes" which, when clicked, displays a table with the contents of the `routes` table,

  - 5 points for properly labeled button
  - 5 points for displaying dialog when cliked
  - 10 points for displaying table contents in the table display dialog
  - 5 points for a button labled "Ok" on table display dialog that, when clicked, returns user to the main screen

- "Stops" which, when clicked, displays a table with the contents of the `stops` table,

  - 5 points for properly labeled button
  - 5 points for displaying dialog when cliked
  - 10 points for displaying table contents in the table display dialog
  - 5 points for a button labled "Ok" on table display dialog that, when clicked, returns user to the main screen

- "Vehicles" which, when clicked, displays a table with the contents of the `vehicles` table, and

  - 5 points for properly labeled button
  - 5 points for displaying dialog when cliked
  - 10 points for displaying table contents in the table display dialog
  - 5 points for a button labled "Ok" on table display dialog that, when clicked, returns user to the main screen

- "Passengers by Date" which, when clicked, displays a table with the contents of the `passenger_data` table ordered by the date column in reverse chronological order.

  - 5 points for properly labeled button
  - 5 points for displaying dialog when cliked
  - 10 points for displaying table contents in the table display dialog
  - 5 points for a button labled "Ok" on table display dialog that, when clicked, returns user to the main screen

### Extra Credit

On the "Passenger by Date" dialog:

  - (10 points) Beside the "Passengers by Date" button should be "Date" label and a text entry that looks for a date in ISO-8601 form, i.e., YYYY-MM-DD
  - (10 points) When a date is provided in the text entry, only passenger data with the same date should be displayed. If no date is given, use the most recent date in the table.


## Tips and Considerations

- Example code is your friend: [https://github.com/datamastery/datamastery.github.io/tree/master/code/gui](https://github.com/datamastery/datamastery.github.io/tree/master/code/gui)
- There's a MySql example that you will find very helpful. Play with it and figure out how to extract from it what you need and this assignment becomes easy. [https://github.com/datamastery/datamastery.github.io/blob/master/code/gui/mysql_browser.py](https://github.com/datamastery/datamastery.github.io/blob/master/code/gui/mysql_browser.py)

## Turn-in Procedure

Submit your `marta_gui.py` file on Canvas. After you turn in your files, make sure to check that you have submitted the correct files that you intended to. It is highly recommended that you submit the assignment early and often in order to avoid incorrect/incomplete submissions near the deadline.

Good luck!