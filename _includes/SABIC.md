# SABIC Introduction to Computing

## Instructor

Christopher Simpkins, Ph.D.
College of Computing


## Course Description

This course will provide background and experience in reading, manipulating, and exporting data for engineering, business and scientific applications.

## Learning Outcomes

Student in the class will achieve the following learning objectives:

(Competency) Students will be able to:

1. Write programs using various data types, and using basic techniques such as assignment, method calls, while loops, for loops, and conditionals.
2. Use and manipulate several language provided data structures such as: Lists, Dictionaries, and Strings.
3. Read and write data to and from text files, both as plain text and in structured formats (such as CSV).
4. Read a textual representation of numerical data and convert it to the appropriate (integer/floating point) data type.
5. Implement simple business or mathematical algorithms (calculating interest payments, averaging a row of data, calculating standard deviation) into a program.
6. Use compound data structures provided by the programming language such as lists, arrays, and dictionaries to hold sequences or sets of data, including two-dimensional (tabular) data.
7. Use objects and associated methods provided by the programming language.
8. Write programs that are easy to understand so that others may modify and improve them.

(Movement) Students will increase their:

1. Familiarity with compound data structures (lists, arrays, dictionaries), including nested data structures (multi-dimensional arrays, etc...) and indexing into multi-dimensional data structures.
2. Speed and accuracy in converting problem statements into programs.
3. Understanding of and ability to quickly use basic program structures such as iteration, conditionals, and function calls due to repeated practice of these concepts.
4. Ability to break a medium sized problem down into smaller parts and solve each sub-problem individually.
5. Ability to test and debug programs.

(Experience) Students will:

1. Practice the process of constructing small (10-100 line) programs from written requirements.
2. Deal with data that may include missing elements or malformed representations.
3. Work in pairs to solve programming problems.

## Requirements

### Grading

No grades are given to workshop students.

### Assignments

Each workshop session will include demonstrations and in-class exercises. Each lesson also includes additional exercises for practice outside of class.

#### Academic Integrity and Collaboration

We expect academic honor and integrity from students. Please study and follow the academic honor code of Georgia Tech: http://www.honor.gatech.edu/content/2/the-honor-code. You may collaborate on homework assignments, but your submissions must be your own. You may not collaborate on in-class programming quizzes or exams.


## Course Outline

This outline applies to Fall and Spring semesters. Summer schedule is compressed into 11 instructional weeks.

* Weeks 1: Computing and Python Principles
* Weeks 2: Values, variables, functions
* Weeks 3: Modules, programs, data structures
* Weeks 4: Control structures
* Weeks 5: Functional programming
* Weeks 6: Classes and objects
* Weeks 7: CSV data
* Weeks 8: Special topics

## Prerequisites

This course is designed for students with little to no programming experience.

## Course Materials

I provide everthing necessary for study outside of class, including material that I write myself and free online resources. The books listed below provide additional material and perspectives.

Note: O'Reilly books listed below are available through Georgia Tech's Safari Onine subscription. See http://www.library.gatech.edu/search/ebooks.php

* Required Text: Introducing Python, by Bill Lubanovic, O'Reilly Media, November 2014.

  * Print ISBN: 978-1-4493-5936-2,  ISBN 10: 1-4493-5936-1
  * Ebook ISBN: 978-1-4493-5935-5,  ISBN 10: 1-4493-5935-3

* Recommended Books:

  * Think Python, 2nd Edition, by Allen B. Downey, O'Reilly Media, December 2015. Available free at http://greenteapress.com/wp/think-python-2e/ and from O'Reilly at http://shop.oreilly.com/product/0636920045267.do
  * Python in a Nutshell: http://shop.oreilly.com/product/0636920012610.do
  * Fluent Python (Advanced): http://shop.oreilly.com/product/0636920032519.do
  * Flask Web Development: http://shop.oreilly.com/product/0636920031116.do
  * Tkinter GUI Application Development Blueprints: http://shop.oreilly.com/product/9781785889738.do
  * Python Data Science Handbook: http://shop.oreilly.com/product/0636920034919.do
  * Programming in Python 3 (2nd edition) : Mark Summerfield - Addison Wesley, ISBN: 0-321-68056-1
  * Dive into Python 3 – Mark Pilgrim – Apress ISBN: 978-1430224150

## Non-Discrimination

The Institute does not discriminate against individuals on the basis of race, color, religion, sex, national origin, age, disability, sexual orientation, gender identity, or veteran status in the administration of admissions policies, educational policies, employment policies, or any other Institute governed programs and activities. The Institute’s equal opportunity and non-discrimination policy applies to every member of the Institute community.

For more details see [http://www.policylibrary.gatech.edu/policy-nondiscrimination-and-affirmative-action](http://www.policylibrary.gatech.edu/policy-nondiscrimination-and-affirmative-action)
