---
layout: homework
title: Homework 5 - MARTA Analysis
---

# Homework 5 MARTA Analysis


## Introduction

In this assignment you'll practice writing to and reading from databases in Python.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking this course, the TA's and the lecturer. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, Canvas ID and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.


## Problem Description

MARTA is considering improvements to their service and they’ve hired you to analyze their data.

## Solution Description

Download [`apcdata_week.csv`](http://data.tweedbathrobe.com/marta/apcdata_week.csv), which contains bus route information for a full week. This data file is 92 MB and contains nearly a million rows of data. In the same directory as this data file write a Jupyter Notebook named `<login-id>-marta-analysis`, where <login-id> is your Canvas login ID, e.g., `jdoe3`. Your notebook should have the following parts:

### Part 0 - Header

Include a Markdown cell at the top of your notebook with your name, Canvas ID and GTID.

### Part 1 - Basic Metrics

For each of the following, include a Markdown cell with the question, and a code cell which computes and displays the answer.

- What is the `stop_name` where the most people got on?
- What is the `stop_name` where the most people got off?
- Which `direction` has the highest passenger traffic? (Passenger traffic is determined by the sum of the number of people getting on at any stop along a route in a given direction)
- Which `direction` has the lowest passenger traffic?
- What is the `route_name` of the most popular route? (The most popular route is the one with the highest average passenger traffic in either direction.)
- Which `date` has the highest passenger traffic?
- Which `date` has the lowest passenger traffic?

### Part 2 - Visualization

Often single answers don't give enough information. For example, perhaps the second-busiest stop is close to the busiest but all others are far behind.

For each of the following, include a Markdown cell with the question/description and a code cell that produces the visualization. Choose a graphic display that would clearly present the information.

- System-wide passenger traffic (ons + offs) totals for each date
- Five busiest stops (ons + offs), ranked buy passenger traffic
- How busy is each route compared to the other routes?
- How busy is each direction (north, south, east, west) compared to other directions?



## Turn-in Procedure

Submit your `<login-id>-marta-analysis.ipynb` file to this homework assignment on Canvas. DO NOT submit the `apcdata_week.csv` data file! After you turn in your files, make sure to check that you have submitted the correct files that you intended to. Do not wait unti lthe last minute to submit your assignment!

Good luck!
