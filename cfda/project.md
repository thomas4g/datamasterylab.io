---
layout: dmcs
title: Final Project
---

# Final Project

In lieu of a written final exam you will do a final project using a range of course topics, especially data analytics. You may work alone or in groups of up to five students -- the larger the group the greater the expectation.

## Requirements

Your project must employ some of the following features:

- Data aquisition, either through a (set of) large data file(s) in CSV, XML, or JSON format, or from a network source using web scraping or web services.
- Transformation of the data and synthesis of information from the data, such as summarizing or relating data.
- Presentation of results, possibly a graphical visualization

## Timeline

- **Proposal**: no later than two weeks before the final exam by Canvas message to me. Your brief proposal should describe:

    - the application you want to write,
    - the data you will use,
    - how you will acquire your data,
    - the format of the data you will consume,
    - what sort of transformation or use of the data you will implement, and
    -

- **Demonstration**: by apppointment some time during the last week of class or final exam week.

- **Final Deliverable**: submit your program code and data files (if applicable) on Canvas by midnight on the last day of final exams.
