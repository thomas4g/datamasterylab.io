---
layout: dmcs
title: CS 2803 - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours located in CoC 133

- Tuesday 13:30 - 15:00
- Thursday 12:30 - 14:00
- Other times by appt.

Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or the TA lab if I'm not expecting anyone.

## TA Office Hours in CoC 107A

- Wednesdays, 13:30 - 16:30
